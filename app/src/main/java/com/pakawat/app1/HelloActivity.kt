package com.pakawat.app1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.pakawat.app1.databinding.ActivityHelloBinding
import com.pakawat.app1.databinding.ActivityMainBinding

class HelloActivity : AppCompatActivity() {
    private lateinit var binding: ActivityHelloBinding
    private lateinit var name : String
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        binding = ActivityHelloBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        name = intent?.extras?.getString(MainActivity.NAME).toString()

        binding.name.text = name
//    FINISH
    }
}