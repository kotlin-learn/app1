package com.pakawat.app1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.pakawat.app1.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        binding.btnHello.setOnClickListener {
            val name =  binding.name.text.toString()
            val studentId = binding.stuId.text.toString()
            Log.d(TAG,"$name $studentId")
            val intent = Intent(applicationContext, HelloActivity::class.java)
            intent.putExtra(NAME,name)
            startActivity(intent)

        }
    }


    companion object{
        const val TAG = "ACTIVITYMAIN"
        const val NAME = "name"
    }
}